<!DOCTYPE html>
<html>
    <head>
<?php include "../include/header_public.php";  ?>
	<link href="statistique.css" rel="stylesheet">
        <title>statistiques jeu</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
	

    <body>
<form method="POST">
        <?php
		include '../include/connexionbdd.php';
		
		?> 
		<h1 class="pagename">Statistiques diverses sur les jeux vidéo</h1>
        <br> 
        <h2>Le jeu le mieux noté</h2>
        <fieldset>
	<div class="note">
        <h2></h2>
		<?php
		$requete="select jeu from(select max(moy) as ma, jeu from(select avg(note) as moy,jeu from jeux_video.donner_avis group by jeu) as moyenne group by jeu order by ma desc) as maximum limit 1";
		//$result=pg_query($dbconn,$requete);
		$result=$connexion->query($requete);
		
		while ($row = $result->fetch()) {
			$id=$row[0];
		}
		
		$requete="select * from jeux_video.jeu where id_jeu=$id";
		$result=$connexion->query($requete);
		while ($row = $result->fetch()) {
			$desc=$row[1];
			$annee=$row[2];
			$nom=$row[10];
			$image=$row[3];
		}
		
		$requete="select round(avg(note)) from jeux_video.donner_avis where jeu=$id";
		$result=$connexion->query($requete);
		while ($row = $result->fetch()) {
			$note=$row[0];
		} 
		?>
		<p>note : <?php echo $note; ?></p>
	</div>
	<div class="test">
	<div>
        <img class="image_jeu" src=../images/jeux/<?php echo $image ?>>        
	</div>
	<div>
	<p class="nom_jeu">Nom du jeu : <?php echo $nom; ?></p>
	<br>
        <p class="annee">Date de sortie : <?php echo $annee; ?></p>
	<br>
        <p class="desc">Description du jeu : <?php echo $desc; ?></p>
	</div>
        </fieldset><hr>
        
		<h2>Le jeu favori</h2>
        <fieldset>
	<div class="note">
        <h2></h2>
		<?php
		$requete="select jeu from(Select jeu, max(jeux)from(select jeu ,count(jeu) as jeux from jeux_video.avoir_pour_favoris group by jeu)as compte group by jeu, jeux order by jeux desc limit 1)as maxi";
		//$result=pg_query($dbconn,$requete);
		
		$result=$connexion->query($requete);
		while ($row = $result->fetch()) {
			$id=$row[0];
		}
		
		$requete="select * from jeux_video.jeu where id_jeu=$id";
		$result=$connexion->query($requete);
		while ($row = $result->fetch()) {
			$desc=$row[1];
			$annee=$row[2];
			$nom=$row[10];
			$image=$row[3];
		}
		
		$requete="Select max(jeux)as jeux from(select jeu ,count(jeu) as jeux from jeux_video.avoir_pour_favoris group by jeu)as compte group by jeu, jeux order by jeux desc limit 1";
		$result=$connexion->query($requete);
		while ($row = $result->fetch()) {
			$note=$row[0];
		} 
		?>
		<p>nombre de favoris : <?php echo $note; ?></p>
	</div>
	<div class="test">
	<div>
        <img class="image_jeu" src=../images/jeux/<?php echo $image ?>>        
	</div>
	<div>
	<p class="nom_jeu">Nom du jeu : <?php echo $nom; ?></p>
	<br>
        <p class="annee">Date de sortie : <?php echo $annee; ?></p>
	<br>
        <p class="desc">Description du jeu : <?php echo $desc; ?></p>
	</div>
        </fieldset><hr>
		
        <?php
		$requete="select jeu from(select max(moy) as ma, jeu from(select avg(note) as moy,jeu from jeux_video.donner_avis group by jeu) as moyenne group by jeu order by ma) as maximum limit 1";
		
		$result=$connexion->query($requete);
		while ($row = $result->fetch()) {
			$id=$row[0];
		}
		$requete="select * from jeux_video.jeu where id_jeu=$id";
		$result=$connexion->query($requete);
		while ($row = $result->fetch()) {
			$desc=$row[1];
			$annee=$row[2];
			$nom=$row[10];
			$image=$row[3];
		}
		$requete="select round(avg(note)) from jeux_video.donner_avis where jeu=$id";
		$result=$connexion->query($requete);
		while ($row = $result->fetch()) {
			$note=$row[0];
		}
		?>
		  
        <h2>Le jeu le moins bien noté</h2>
        <fieldset>
        <div class="note">
        <h2></h2>
	<p>note : <?php echo $note; ?></p>
	</div>
	<div class="test">
	<div>
        <img class="image_jeu" src=../images/jeux/<?php echo $image; ?>>        
	</div>
	<div>
	<p class="nom_jeu">Nom du jeu : <?php echo $nom; ?></p>
	<br>
        <p class="annee">Date de sortie : <?php echo $annee; ?></p>
	<br>
        <p class="desc">Description du jeu : <?php echo $desc; ?></p>
	</div>
	</div>
        </fieldset>
</form>
<?php
	include '../include/footer_public.php';  ?>
    </body>
</html>

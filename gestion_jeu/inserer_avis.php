<?php
session_start();
include '../include/connexionbdd.php';

$pseudo=$_SESSION['pseudo'];
$note=$_POST['note'];
$avis=$_POST['avis'];
$jeu=$_POST['jeux'];

//recherche de l'id du joueur dont le pseudo a été récupéré  (il faudra par la suite remplacer par variable de SESSION
$requete = "SELECT id_utilisateur FROM jeux_video.utilisateur where pseudo='".$pseudo."';";
$resultat = $connexion->query($requete);
$id = $resultat->fetch();

if ($id['id_utilisateur']==null) {
	header("Location: donner_avis.php?erreur=pseudo inconnu !! ");
}
else {
	// recherche s'il existe déjà une ligne pour cet utilisateur et ce jeu (pk de la table donner_avis)
	$requete = "SELECT date FROM jeux_video.donner_avis where aviseur=".$id['id_utilisateur']." and jeu=".$jeu.";";
	$res = $connexion->query($requete);
	$recup_date = $res->fetch();
	
	// si pas de date (donc pas de ligne utilisateur/jeu déjà existante on peut insérer l'avis
	if ($recup_date['date']==null)
	{
		//echo "date normalement vide :".$recup_date['date'];
		$sql = "INSERT INTO jeux_video.donner_avis (jeu,aviseur,avis,date,note) VALUES (?,?,?,?,?)";
		$statement = $connexion->prepare($sql);
		$statement->execute([$jeu, $id['id_utilisateur'], $avis, date('Y-m-d'),$note]);		

		header("Location: donner_avis.php?message=Insertion avis ok !!");
	}
	else { // sinon 
		echo "cas date non vide ";
		header("Location: donner_avis.php?erreur=Vous avez déjà donné votre avis pour ce jeu !! ");
	}
}

